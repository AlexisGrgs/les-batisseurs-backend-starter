import readFile from "../utils/readFile"
import camelCase from "lodash/camelCase"

export function csvToJson(file) {
  const [headerLine, ...lines] = file.split("\n")
  const headers = headerLine.split(";")
  return lines.map(line => {
    const cells = line.split(";")
    const tmpObject = {}
    for (let i = 0; i < cells.length; i++) {
      tmpObject[camelCase(headers[i])] = Number.parseInt(cells[i])
    }
    return tmpObject
  })
}

export async function importBuildings() {
  const buildingsFile = await readFile(
    `${__dirname}/../ressources/buildings.csv`
  )
  return csvToJson(buildingsFile)
}

export async function importWorkers() {
  const buildingsFile = await readFile(`${__dirname}/../ressources/workers.csv`)
  return csvToJson(buildingsFile)
}
