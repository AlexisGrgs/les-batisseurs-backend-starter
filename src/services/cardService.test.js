import * as cardService from "./cardService"

describe("csvToJson", () => {
  test("should transform a csv to a javascript object", async () => {
    const testCsv = `\
wood;stone;victory point
1;2;3
4;5;6`

    expect(cardService.csvToJson(testCsv)).toEqual([
      { wood: 1, stone: 2, victoryPoint: 3 },
      { wood: 4, stone: 5, victoryPoint: 6 }
    ])
  })
})
